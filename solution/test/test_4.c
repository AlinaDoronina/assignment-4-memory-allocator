#include "../src/mem.h"
#include "../src/mem_internals.h"
#include "test_4.h"

static uint8_t* init_and_debug_block(size_t query) {
    void* addr =_malloc(query);
    if (addr!=NULL) return (uint8_t*) addr;
    else return NULL;
}

static void the_memory_has_run_out_the_new_memory_region_expands_the_old_one_handler() {
    printf("TEST 4: the memory has run out, the new memory region expands the old one.\n");
    debug_heap(stdout, HEAP_START);

    uint8_t* addr = init_and_debug_block(1);
    if (addr==NULL) printf("TEST4 failed.");

    uint8_t* addr2 = init_and_debug_block(30);
    if (addr2==NULL) printf("TEST4 failed.");

    uint8_t* addr3 = init_and_debug_block(REGION_MIN_SIZE);
    if (addr3==NULL) printf("TEST4 failed.");
    debug_heap(stdout, HEAP_START);

    //prepairing heap for the next tests
    _free(addr); _free(addr2); _free(addr3); printf("\n");
}

void test_4() {
    the_memory_has_run_out_the_new_memory_region_expands_the_old_one_handler();
}