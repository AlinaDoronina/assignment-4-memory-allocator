#include "../src/mem.h"
#include "../src/mem_internals.h"
#include "test_5.h"
static uint8_t* init_and_debug_block(size_t query) {
    void* addr =_malloc(query);
    if (addr!=NULL) return (uint8_t*) addr;
    else return NULL;
}

static void memory_expansion_in_a_different_address_handler() {
    printf("TEST 5: memory expansion in a different address.\n");
    alloc_region((uint8_t*)0x4045000, 3);
    debug_heap(stdout, HEAP_START);

    uint8_t* addr = init_and_debug_block(1);
    if (addr==NULL) printf("TEST4 failed.");

    uint8_t* addr2 = init_and_debug_block(30);
    if (addr2==NULL) printf("TEST4 failed.");

    uint8_t* addr3 = init_and_debug_block(REGION_MIN_SIZE*4);
    if (addr3==NULL) printf("TEST4 failed.");
    debug_heap(stdout, HEAP_START);

    _free(addr3); debug_heap(stdout, HEAP_START);

    // //prepairing heap for the next tests
    _free(addr); _free(addr2); printf("\n");
}

void test_5() {
    memory_expansion_in_a_different_address_handler();
}
