CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
MAINDIR=solution
SRCDIR=solution/src
TESTDIR=solution/test
CC=gcc

all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/main.o $(BUILDDIR)/test_1.o $(BUILDDIR)/test_2.o $(BUILDDIR)/test_3.o $(BUILDDIR)/test_4.o $(BUILDDIR)/test_5.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/mem.o: $(SRCDIR)/mem.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/mem_debug.o: $(SRCDIR)/mem_debug.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(MAINDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@
	
$(BUILDDIR)/test_1.o: $(TESTDIR)/test_1.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/test_2.o: $(TESTDIR)/test_2.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/test_3.o: $(TESTDIR)/test_3.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/test_4.o: $(TESTDIR)/test_4.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/test_5.o: $(TESTDIR)/test_5.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)

